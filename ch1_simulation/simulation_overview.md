# Introduction to Simulation
In this chapter, we want to explain what simulations are in the first place. During the first section, we explain four aspects of simulations in more detail that to our mind are most important for historians to grasp what simulations are all about.

The second section will deal with what goals simulations are used for, primarily limited to the humanities and history. We will give examples for all the uses we identify, so you get a look into what studies there are around.

We will also cover some different simulation approaches and types of simulation methods in the last section, although we mostly cover the ones we considered using in the ModelSEN project.

Click the section you are interested in or just start from the top:

```{tableofcontents}
```
