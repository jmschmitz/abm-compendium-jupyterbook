
# Types of simulation methods
When building a simulation model, it is useful to differentiate your approach from others to make it clear in an instant what other researchers can roughly expect from your simulation model. You also might be needing a particular simulation approach for your own research, as different simulation approaches have different concepts and problems attached to them. 

Unfortunately, there are a number of sometimes conflicting ways in which simulation models are differentiated. Some authors differentiate them by research goals, some by specific methodology, by implicit or explicit assumptions or theoretical underpinnings of a model, by the research subject, or - more specifically - the type of system or the type of dynamics the model tries to simulate (examples for various differentiations are available in {cite:t}`davidsson_types_2017` or {cite:t}`scharnhorst_models_2012`). There are some general typologies that are widely understood and used, though. Here we mainly want to present a small selection of general methods that we thought of using while developing our model at the **ModelSEN project**.

```{admonition} Example
The decision which method is best for your research has many variables. Let's look at a simple example. 

You might want to understand an economic phenomenon, but there are different possible approaches to it: you may want to chose a more bottom-up approach by modeling primarily individual consumers that interact, or you might want a more top-bottom approach that focuses on streams of goods and fluctuations in prices according to generalised supply and demand. Maybe you primarily want to model a physical environment in which that economic process took place, such as a city or a river in its historic form. All of these approaches would differ wildly in the tools you can use, the problems you are faced with, the data you need and also how you can communicate with your audience. Some approaches might be more suited to certain questions than others, but a lot of the time, the decision is also down to your theoretical and epistemological preferences. 
```

## Dynamical and Evolutionary Models
As mentioned in the [dedicated project page](../ch4_ModelSEN/ModelSEN_abm.md), we want to model networks of interacting scientists or letter writers - thus, we want to model interacting agents that influence each other, their environment, and eventually change over time themselves. Not all simulation methods focus on interacting agents, though, which means only some types of simulation methods might be useful for the ModelSEN project. 

The class of approaches we deemed most promising for this project are so-called dynamical or evolutionary models. As the naming suggests, these models are primarly focused on changes within the system and to highlight how these changes work exactly. They often are inspired by a evolutionary theory - not in a social-darwinist sense, but meaning that systemic change is often not the result of planned action, but of incremental decisions, usually involving interactions, which create emergent and potentially very dynamic effects.

Here are some of the dynamical and evolutionary modelling approaches we discussed during the course of the project:

### Selection Dynamics
These models focus on the selection or growing popularity of competing ideas and systems, e. g., the winner-takes-all markets for standards of technologies or scientific theories. This approach usually ignores or omits the emergence of new ideas or strategies. 

See for example {cite:t}`safarzynska_beyond_2011,smaldino_natural_2016`.

### Evolutionary Computation
These are search models whereby new products or ideas develop from existing ones. It usually consists of agents who combine and experiment with existing elements to form new ones. It has the advantage that it can model the appearance and dissemination of new ideas, but the emphasis is much less on the individual, heterogenous actors who interact and who can adapt their behaviour. 

See for example {cite:t}`woodard_modeling_2012`.

### Multi-agent Systems
In these, multiple agents act and interact according to predefined rules but also with the capacity to adapt these rules in response to interactions with other agents or with the environment. The goal of these systems is often to model emergent systemic behaviour that only appears from the interaction and is not prescribed in the model itself.

**Agent-based modeling** is one approach to Multi-agent Systems (sometimes the terms are - falsely - used synonymous) and its the one we chose for our research project in the end. Read more about what Agent-based Modeling is exactly [in the next chapter](../ch2_abm/abm_overview.md).

## Other popular types and similar approaches

### System Dynamics
Modeling a phenomenon as system of stocks and flows, for example the flow of goods or currency between producers and consumers. The famous world3 model that was the basis for the *Limits to Growth* report is an example of system dynamics research. 

Have a look at this website for a more in-depth explanation and examples: {cite:t}`system_dynamics_society_what_2022`.

### 3D-Modelling / Reconstruction
3D-Modeling a physical space to recreate the look (and perhaps even feel) of a location, or as a prerequisite for other types of simulation. Using written accounts, blueprints, maps, geospatial/archaeological/envrionmental data. Often using dedicated 3D-modeling software, separate from scientific simulation software. 

See for example {cite:t}`johanson_making_2015,wall_recovering_2014,bonde_constructiondeconstructionreconstruction_2017`.

### GIS-based modeling
Using geoinformation-systems and data to model an environment, for example landscapes or regions. Data examples are maps, gazeteers, archaeological data, existing geospatial databases (e.g., wikidata).

See for example {cite:t}`travis_historical_2020,izquierdo_combining_2017,herweijer_north_2006`.

**********

### Section Bibliography

```{bibliography}
:filter: docname in docnames
```