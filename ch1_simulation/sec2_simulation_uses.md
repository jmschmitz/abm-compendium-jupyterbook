
# Uses and goals of simulation methods
Simulations have been used with varying goals in different areas of research. In the humanities, simulations are usually used as an analytical tool, partly as a didactic tool, and they are themselves objects of research. In fields such as economics, sociology, or political sciences simulations are also used as a tool to forecast developments. 

## As an analytical tool
For the purposes of this book as well as for the ModelSEN project, using simulations as an analytical tool is their most important application.

Simulations can be used to study a given process or system in-depth, e.g, to try and understand which elements of a historical process are the most important or how some factors of this process need to behave to reach a certain outcome (“why”- and “how”-questions). We might have a general idea of how a historical process transpired - a hypothesis or even a theory - and could try to model events accordingly. Then we could see if the outcomes of the simulation match our expectations. This can be both regarding the hypothesis or theory itself - the internal consistency of the theory - as well as historical sources and the supposed historical outcomes - thus the external consistency of the theory. 

On the other hand, we might want to come up with an entirely new theory or hypothesis of how a historical process unfolded. In that case we could try to set up the model according to our judgements as historians and iterate on these according to the consistency of the model in relation to our sources and perceptions of reality. We would have to find a point at which to conclude that what we see in the model might constitute a plausible perspective on historical events.

Perhaps surprisingly, this process is in essence quite close to how historians work already. Historians try to interpret and connect sources in a way that produces a plausible version of historical reality, continuously reiterating and reformulating their interpretations and judgements according to the sources available. This is the well-known hermeneutic circle that is still the bedrock research methodology for many historians. The main (and of course crucial) difference to simulations is that the judgements historians usually form only in their head or in natural language on paper now have to be articulated entirely in formal language at some point.

Relating to this observation, simulations have also been described as "hermeneutic tools". Simulation methodology "flips and mirrors the hermeneutic circle such that the whole thing looks like a figure-8" {cite}`gavin_agent-based_2014` ({ref}`see Fig. 1 <gavin-lemniskate>`). You can find more on why this methodology is similar to the hermeneutic circle, or "hermeneutic figure-8" in Gavins words, in the chapter on [Agent-based modeling in practice](../ch3_practice/practice_overview.md)

```{figure} ../images/gavin-lemniskate.png
---
name: gavin-lemniskate
align: left
---
 Gavin's "hermeneutic figure-8" of an idealized modeling and simulation process {cite}`gavin_agent-based_2014`
```

Some examples of simulations as analytic tools in history and the wider humanities include: {cite:t}`madge_assessing_2019,ewert_mittelalterlicher_2018,hoenen_simulation_2014,murgatroyd_modelling_2012`

## As a didactic tool
Simulations can also be used to demonstrate scenarios, mechanisms of human interaction under certain constraints, or convey certain perspectives for a didactic end. Simulations as didactic tools are important for Public History, general education, and for science communication but also play a role for higher education and professional training. Analogue simulations that heavily rely on roleplaying of participants has been used for a long time, sometimes for students, but also for training purposes or even for decision makers (two historical examples would be *war games* or the *inter-nation simulation*: {cite}`mchugh_eighty_1969,crow_study_1965`). 

These kinds of simulations have been augmented by computers for decades, with some now being replaced by computer simulations entirely. Especially with the development of more powerful VR-systems in recent years the opportunities of immersive 3D-Modelling of historical environments or scenarios are explored more and more in the academic context. In popular practice, historical simulations played a role in games for decades as well, both as a backdrop for entertainment but also with simulation gaming being a vehicle for historical education {cite}`wright-maley_digital_2018`.

Some literature on simulations as didactic tool in history and the wider humanities include: {cite:t}`vlachopoulos_effect_2017,johanson_making_2015`.

## As research subject
The methodology and epistemology of simulation methods, the evaluation and development of tools and standards of simulation, and of course the history of simulations as a scientific method are a research field in itself, primarily for philosophers and historians of science, but essentially for any practitioner of the method in some capacity. We will be getting to at least some questions in relation to Agent-based modeling for historians [in another chapter](../ch3_practice/caveats.md). 

Some literature on simulations as research subject in history and the wider humanities include {cite:t}`scheuermann_simulation_2020,barcelo_computer_2012,pias_epistemology_2011,hegselmann_modelling_1996`.

## As a forecasting tool
Forecasting developments of certain systems has a long history and plays an important role in current simulation sciences, be it regarding climate, spaceflight, or demographic simulations. As we briefly mentioned above and should be evident, historical science have limited use for forecasting since they look into the past and not into the future. However, there might be an argument to engage with the forecasting uses of simulations from an epistemological point of view even for historians, although reviewing the discussion on this problem would be too much for this introduction. 

*************

### Section Bibliography

```{bibliography}
:filter: docname in docnames
:style: plain
```