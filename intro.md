# Overview

This jupyter-book is part beginner-friendly introduction to core concepts of Agent-based Modeling and Simulation (ABM for short) and part introduction to the ABM-aspect of the [**ModelSEN** project](https://ModelSEN.mpiwg-berlin.mpg.de/). You may find this introduction useful if you are a researcher or student who is unfamiliar with simulation methods in general or ABM specifically, or if you are already considering using ABM but don't know where to start. We want to offer you guidelines, discuss common problems, and point to some key debates on how this method fits into digital history. 

Finally, this is also a place where you can learn more about the general methodology and thinking behind our agent-based models in the ModelSEN project.

# Where to start?

Skip to the [overview of the ModelSEN project](ch4_ModelSEN/ModelSEN_abm.md) if you are already familiar with ABM or just want a quick look into what we are doing with ABMs in the ModelSEN project. (Note: as of this version of the jupyterbook, our models are still in a relatively early phase and not extensively discussed here).

Skip to the [introduction of simulation methods](ch1_simulation/simulation_overview.md) if you are completely new to that field and want a general introduction.

Skip to the [introduction of Agent-based modeling](ch2_abm/abm_main.md) if you have at least some basic understanding of simulations and want to get more specific.

Skip to the [section about ABM in practice](ch3_practice/practice_overview.md) if you have basic familiarity with the method and now want to start building your own ABMs.

Skip to the [discussion of caveats and problems of ABM](ch3_practice/caveats.md) if you are familiar with ABM in general and now want to explore the some challenges of and discussions around the method in-depth.

Skip to the [discussion of the future of ABM in historical sciences](caveats:desiderata) if you are familiar with ABMs and want to know how we think the field could and should develop and how a community of practice could be established.

Skip to our ['Mesa' Tutorial](ch5_mesa/mesa_overview.md) if you are familiar with ABM methodology and want to get some actual ABM-coding experience.

# Full table of contents

```{tableofcontents}
```
