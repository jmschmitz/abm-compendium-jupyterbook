# ABM in the ModelSEN Project

## What is the ModelSEN Project?
'ModelSEN' is a shorthand for: 'Socio-epistemic networks: Modelling Historical Knowledge Processes'. 
From [our webpage](https://ModelSEN.mpiwg-berlin.mpg.de/):

    "The growing interconnectedness of knowledge in the fast communicating nodes
    of the internet leads to an inherent dynamic production of knowledge. 
    In order to profoundly understand the knowledge system resulting from these
    dynamics, it is crucial to also understand the development of
    knowledge systems in their historical unfolding.

    To achieve this understanding it is necessary to obtain a description of
    knowledge systems development that is not only rigorous from the historical 
    perspective, but also formalized from the mathematical standpoint, covering 
    the dynamical interactions of people, materials and ideas."

The project is situated at [Department I of the Max-Planck-Institute for the History of Knowledge](https://www.mpiwg-berlin.mpg.de/dept-one), which is headed by Jürgen Renn and is aimed at researching "Structural Changes in Systems of Knowledge". 

## How and why do we apply Agent-based Modeling?
In the ModelSEN Project, we are currently working on two models: one model of interactions between scientists of different institutions in the historical field of General Relatively, mapping the dynamics of knowledge transfer and the generation of new concepts, and another one simulating the evolution of letter networks in the *Republic of Letters*, called 'LetterSpace', which is primarily aimed at understanding through which processes the source material is preserved or lost. 
 
Both models are still in a relatively early, experimental stage at the moment of writing, although the preliminary 'LetterSpace' model is already part of our evolving [Scientific Communication (SciCom) package](https://scientificcommunication.readthedocs.io/en/latest/), where all models of the project will be published at some point. 

We were motivated to use ABM for different reasons. We wanted to offer descriptive data on the relationship between scientists, topics, journals, places, etc., which is realised in the network-analysis part of the project (see fore example our project's [case studies](https://ModelSEN.mpiwg-berlin.mpg.de/case-studies/) as well as other parts of the [compendium](https://ModelSEN.mpiwg-berlin.mpg.de/compendium)). There we generate static snapshot picture of social influence or the co-occurrence of topics at different points in time. 

However, we wanted to move beyond this static perspective to a more dynamical one, and towards the causality of the processes we observe in our case studies. To quote {cite:t}`pearl_book_2018`: 
    
    "While probabilities encode our beliefs about a static world, 
    causality tells us whether and how probabilities change when the world 
    changes, be it by intervention or by an act of imagination."

ABM offers use an avenue to look at the essence of the changes in the scientific and social networks we observe. We get a perspective on underlying processes and emergences and, crucially, on the meso-level between the micro-level of individuals' actions in the networks of science and the macro-level of overarching shifts in scientific fields, concepts, the modes of research and communication. It also allows us to look into meta-processes, such as for what reasons source material survived or was lost, potentially shedding light on the blind spots of historical networks.