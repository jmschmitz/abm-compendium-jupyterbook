# 'Mesa' Tutorial

[Mesa](https://mesa.readthedocs.io/en/stable/) is a python package for building and simulating agent-based models. It offers a variety of features and is generally quite open for adding your own functionalities. In the following section, we want to give you a relatively short tutorial on some of the basics of `mesa`. 

```{admonition} Other Programming Languages
We listed a number of programming tools [in this section here](content:models:languages), many of which are especially created for ABM. 

Special tools like NetLogo might be more beneficial for you if you don't have prior experience with python. In this case, we recommend you have a look at NetLogo, for which there are extensive and beginner-friendly [video tutorials available on youtube](https://www.youtube.com/playlist?list=PLF0b3ThojznRKYcrw8moYMUUJK2Ra8Hwl).

```

This tutorial is largely based on the original introductory tutorial you can find in the `mesa` documentation linked to above. We mainly reframed the tutorial to be about a topic that is more interesting for many historians while the original tutorial is about wealth distribution (of course also of interest for economic historians!). 

In the `mesa` documentation, there are also [more advanced tutorials](https://mesa.readthedocs.io/en/stable/tutorials/adv_tutorial.html) that cover more features (especially the real-time visualization options), so we heavily recommend to have a look at those tutorials when you are done with this one!

Originally, this tutorial was preapared for an in-person workshop, which is why some features are not covered. Again, we recommend to take a look at `mesa`s docs.

## 