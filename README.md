[![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/git/https%3A%2F%2Fgitlab.gwdg.de%2Fjmschmitz%2Fabm-compendium-jupyterbook/main)

# Agent-based Modelling for Historians

This jupyter-book is part beginner-friendly introduction to core concepts of Agent-based Modeling and Simulation (ABM for short) and part introduction to the ABM-aspect of the [**ModelSEN** project](https://ModelSEN.mpiwg-berlin.mpg.de/). You may find this introduction useful if you are a researcher or student who is unfamiliar with simulation methods in general or ABM specifically, or if you are already considering using ABM but don't know where to start. We want to offer you guidelines, discuss common problems, and point to some key debates on how this method fits into digital history. 

We also provide a short tutorial on the python package `Mesa`, with which you can start building your own models.

Finally, this is also a place where you can learn more about the general methodology and thinking behind our agent-based models in the ModelSEN project.

Use the binder feature to open an interactive instance of this jupyter book.