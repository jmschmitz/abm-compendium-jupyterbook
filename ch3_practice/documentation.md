
# Documentation and Standards for ABMs

## Why documentation standards for ABM?
Clearly and systematically documenting for ABM is as important as for any other programming method. It has essentially three main goals: communication, replication, and comprehension {cite}`grimm_documenting_2017`.

Due to their complexity, simulation models are sometimes quite hard to communicate, and ABMs especially so. It can be difficult for an external person to grasp all the intricate relationships between entities in the model, all the more so if they don't have a technical understanding of the programming languages and tools used. Standards make it possible for all participants of a scientific debate to compare models and ascertain their value in relation to each other, right down to the technical level, if some researchers want to replicate a model that is presented to the community. 

Apart from these communicative resasons, standards can also be useful for guiding your own research process, especially when you are a beginner using ABM and don't quite know where to start. It is useful for researchers of any level, though, as the model expands and increases in complexity, to still have a detailed schema to orient yourself to.

## Guidelines for ABM Documentation
There are some general tipps you can follow before delving into a proper standard. Niki Popper and Philipp Pichler describe the following guidelines in a handbook on ABM for Archaeology by {cite:t}`wurzer_agent-based_2015`:

1) Adequacy and Structure: The level of detail in the documentation should be proportional to the importance of the documented entity. Highly detailed topics should be structured into a hierarchy of gradually increasing complexity.

2) Simplicity: Whenever possible, English documentation should be done in Basic English, a constructed language of 850 words which has a simplified grammar and is designed for the communication of complex thoughts. As a matter of fact, Basic English is easily translated (manually and automatically) and can serve a large international community as a basis for collaboration.

3) Clarity: A glossary or definition part may help to avoid ambiguities and shorten the text. However, this also carries the risk of over-formalisation. Examples can help to lighten up the text and illustrate an idea intuitively rather than formally.

4) Audience: Who needs to know what? There are several roles within an M&S project (e.g. users, model developers), each being interested in a different aspect (e.g. archaeological statements, technical descriptions). Making this explicit, possibly through use of formatting (italics, grayboxes, etc.) and labels, can help an interested user in picking out truly interesting information rather than being forced to read everything. {cite}`wurzer_reproducability_2015`.

## Example: The ODD protocol
One of the most discussed and furthest developed standards to communicate ABMs is the so-called 'Overview, Design concepts and Details' (ODD) protocol {cite}`grimm_odd_2020`. It consists of a kind of questionnaire researchers can fill out to cover the most important details of their model in a systematic way. Since its first development in 2006, it has steadily been expanded to account for criticism, mostly about missing questions and modelling aspects in the questionaire. There also have been offshoots/extensions of the protocol like the ODD+D Protocol (the additional D stands for 'Decision') {cite}`muller_describing_2013`. This extension might be of particular relevance for historical sciences, as it features a more detailed explanation of the modeling of agents. 

```{figure} ../images/odd+d-structure.png
---
name: odd_structure
align: left
---
 An overview of the different sections of the ODD+D protocoll, detailing the subsection about agent behavior {cite}`muller_describing_2013`.
```

While ODD is widely accepted also in Archaeology and Sociology, it must be noted that it was developed mainly by ecologists and researchers from other more natural than social sciences. Special consideration for matters of historical inquiry, e.g., source criticism, are not acknowledged in the standard. 

How ABM standards might be developed for historical sciences and what some other desiderata are is discussed in the following section on [caveats, challenges and desiderata in ABM for historians](../ch3_practice/caveats.md).

**********

### Section Bibliography

```{bibliography}
:filter: docname in docnames
```