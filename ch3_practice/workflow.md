# Example Workflow

Modeling and simulation workflows are also called "lifecycles" by some or - as we have learned in a previous chapter - are imagined as "hermeneutic figure-8" {cite}`wurzer_reproducability_2015,gavin_agent-based_2014`. This is to highlight the iterative, evolving, and experimental nature of simulation modeling. It is important to remember that it's always possible to extend the "lifecycle" and keep working on a particular model.  

The key - as in all research projects - is therefore to formulate a concise research question and set up a good project plan. Once that groundwork is laid, you can initiate the first steps of your model's lifecycle.

```{note}
The following workflow is only a suggestion - adapt it as seems necessary or convenient for your research project! 
```

## Conceptualisation
The first step of modeling is usually creating a conceptual model. This might include a number of steps that are typical of historical research anyway, like researching and interpreting sources as well as developing and ordering thoughts. Think of it as a way to systematically order and explicate the objects and subjects of your interest, their interrelations and your thoughts about possible implications. You will revisit your conceptual model and reiterate on it often, so it's good practice to not overthink the first iterations of your conceptual model.

1) Think about the most important entities and processes in your system of interest. Remember your project scope and think about how these entities and processes fit into it, ie., what elements might be crucial and which might be nonessential.

2) Start modeling and linking those entities and processes conceptually. There are some tools that can help you visualise the rough connections and properties of your entities, like [Miro-Boards](https://miro.com/app/), [diagrams.net](https://app.diagrams.net/?src=about), or any other mindmapping tool of your choice. Of course, mapping them out by hand is also possible, but a digital tool might be more useful for later steps. 

3) Think about various ways these entities could be modeled logically (ideally already thinking in terms of the programming language of your choice); think of theoretical frameworks or empirical sources that justify one modeling decision over another.

4) Start formalizing the entities and relations. There are various ways to start formalizing your model to match more closely the form it will take in the programming environment. One useful approach is the Unified Modeling Language (UML) that offers a range of standardised diagrams to represent parts of a model, like the so called activity or class diagrams, that can be used to model entities and behaviors in ABM {cite}`bell_introduction_2003`. 

As we have said above, you will revisit your conceptual model and change it in response to the behavior of the simulation, new sources you might find or discussions you will have along the way. For this reason, you might want to formalize, implement, test, and observe the implications of one feature before you do the next. 

## Formal Modeling
Now starts the process of translating your roughly formalized model into actual, working code. 

```{admonition} Which programming language/tool is best for beginners?
We listed a number of programming tools [in this section here](content:models:languages), many of which are especially created for ABM. 

The [programming suite and language NetLogo](https://ccl.northwestern.edu/netlogo/) might be a good choice if you want to start out with ABM. It was primarily designed for a didactic context and thus is relatively simple to learn, there are a lot of courses and tutorial materials available online, and it comes with a number of example models from different scientific fields you can play with to get familiar with the tool. For example, see [this comprehensive youtube course by the Santa Fe Institute's "Complexity Explorer" programme](https://www.youtube.com/playlist?list=PLF0b3ThojznRKYcrw8moYMUUJK2Ra8Hwl)

You can also use our ['Mesa' Tutorial](../ch5_mesa/mesa_overview.md) as a starting point! There you will learn how to use a Python-based package to build ABMs. This requires some Python familiarity, though.

```

1) Start setting up your model's environment, ie., the non-acting entities that you want to have in your system. These might only be represented by simple, steady variables, but could take any number of forms. You can keep it simple here at first: only implement what you will need for the next step to keep track of interactions.

2) Start implementing your acting entities, ie., the actual agents. One sensible way to go about this is to break down the features (i.e., behaviours) of your agents and implement them one by one, each time testing how the whole model reacts to the changes (and of course if your agents are functioning in a way you intended).

## Simulation, model analysis, and testing
Testing your model first of all means running the simulation and seeing how the model reacts to the changes / features you introduced. At the end of the workflow, there should be a dedicated phase of systematical and computerized testing of your model. 

1) The most important step for testing and analysis is experimentation. Simulate your model often to see how the changes you made recently impact its dynamics. 

2) Keep thinking about your initial assumptions, conceptualisations, and the behavior of your model when it is simulated. Do you find any glaring inconsistencies? If yes, then go back to modeling. 

3) As hinted at above, this practice might also entice you to rethink your theoretical or empirical basis as well as your conceptual model. It is good practice to act on these changing perceptions - the initial modeling plan should not be set in stone (although time and resource constraints obviously need to be considered and communicated). 

4) You should keep notes of your thoughts and modeling process. Keeping track of it is useful for yourself to understand how you got to the final model but also might be important for communicating your results.

5) After implementing all the features that you want included, after testing them step-by-step and reiterating on your model and its entities, it is time to do more formalised testing. One important aspect of this is sensitivity testing, which means determining how the elements of your model are actually working together on a computational level and testing if it's working the way you intended. 

There are a number of technical methods you could use for sensitivity testing and other ways to verify your model, for example Markov chain analyses, Monte-Carlo simulations, mean dynamics, stochastic stability analyses and diffusion approximations, and more. 

For a more in-depth explanation of validation and verification as well as a suggested "lifecycle" for archaeological ABMs, see {cite:t}`wurzer_reproducability_2015`.

*************

### Section Bibliography

```{bibliography}
:filter: docname in docnames
```