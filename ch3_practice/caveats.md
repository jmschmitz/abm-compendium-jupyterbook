
# Caveats, discussions, and desiderata
In this section we want to highlight caveats using ABM, ongoing discussions, as well as general desiderata for the application of ABM in historical sciences. Some of these are specific to ABM in general, some are especially relevant for historical sciences. In any case, much of the issues and discussions surrounding ABM are fluid and ongoing, or even just starting.  

## What are caveats in using ABM in history?
There are certainly a lot of aspects that could be discussed in terms of problems, challenges, and caveats of using ABM. Here we want to highlight 3 challenges for ABM in historical sciences that seem particularly important to us.

### Overwhelming Variability
Most importantly, while ABMs give researchers a great deal of freedom and possibilities in modeling agents and their environments, this freedom can quickly become overwhelming, both for the actual modeling as well as for analyzing the data and communicating results. This is a problem that is inherent to the method, but can be alleviated somewhat in the future by establishing standards of modeling and communication, as is done in other fields of research that use ABM (see the [section on standards](../ch3_practice/practice_overview.md)). 

Even inside of the confines of your research question there can be a number of ways you may model a phenomenon or system. To help you decide, a well formulated, concise research question does help in the first place. A clear modeling methodology and a solid theoretical basing can help you make modeling decisions (as well as help justify them to the community afterwards). Additionally, both can help to avoid "feature-creep" in your model - the process of constantly coming up with new elements you *could* implement in your model but really shouldn't to not overcomplicate your model or strain your project's resources. 

However, even if standards of modeling and communication would be established (which they aren't quite yet even in fields regularly practicing ABM), ABMs have an inherent complexity. It is an open discussion how visualisations, conceptual representations or proper use of natural language can aid in communicating ABMs. Crucially, no best-practises of ABM in history exist, so every historian wanting to use ABM must at least to some extent trailblaze their way through the vast amount of potentially misleading possibilities ABM presents. 

### Sources for ABM and Simulated Data as Source
Sources are the integral unit of inquiry for historians, and they are the basis most of them draw their interpretations and conclusions from. ABMs (and simulations in general) are in a weird tertiary position between source and interpretation. They don't represent interpretations of reality themselves, only opportunities for historians to take a new perspective and come up with new interpretations. 

In that regard they kind of function like sources - only that the data generated in simulations is not actually source material directly related to a historical reality. Only the input data is grounded in empirical material, the actual sources, but it plays a role mostly as calibration for the model itself. 

So how do we treat historical sources as data for ABM on the one hand and how do we treat the resulting simulated data as kind of synthetic source itself? 

Results of ABM are - like results of many other digital historical methods - not a true representation of real objects and sources, but more abstractions of/propositions about them {cite}`gavin_agent-based_2014`. They are closer to secondary than primary sources, although in the case of ABM, a real world phenomenon is modeled (at least partially) according to primary sources. 

Original source material as input for ABMs also presents a challenge. Other digital methods in the historical sciences have shown that ambiguities in the source material regarding concepts of person, place, time, or even attributions (of action, property, truth, etc.) can be challenging for formal representation. This is also true for ABM, although the method might even be a tool to experiment with these ambiguities and possibly help understand them. 

The actual nature of this kind of source, the epistemological implications of using them in historical research, as well as the proper translation of source material into ABMs will need to be discussed further. In the end, a proper source, data, and method criticism for simulations needs to be developed akin to other recent efforts in relation to digital hermeneutics {cite}`fickers_update_2020`.

### Lacking Concepts of Agency in History
Another caveat of applying ABM to historical research is how to formalize (and therefore theorize) historical human behavior. 

Formal descriptions of human behavior are uncommon in current historical scienes or even, as some argue, antithetical to it (as does for example {cite:t}`scheuermann_simulation_2020`). Historians will need to find approaches to the formalization of human behavior that take into account (although not necessarily need to conform to) theoretical and epistemological traditions of the field (see the desiderata section below).

Historical sciences could also draw on existing theories of behavior from other fields of inquiry, such as sociology, psychology, economics, or others. Some examples could be the theories of Social Practices (a more structuralist sociological theory that puts emphasis on culturally, socially embedded practices more than individual decisions {cite}`shove_dynamics_2012`), habitual behavior (a hot topic of social-psychological research in the last 20+ years {cite}`wood_habit_2017`), bounded rationality (an extension of classical economic concepts of rational agents that constrains individuals with imperfect knowledge, motives, and other elements that "bound" otherwise rational behavior {cite}`wheeler_bounded_2020`), or opinion dynamics (a model of the forming of opinions - social, normative, or scientific - inspired by physical principles {cite}`moussaid_social_2013`).

One historical tradition that needs to be discussed inside the field, though, is the assumption of contingency in the actions of historical individuals. Once approached from a modeling perspective, it becomes clear that this concept is not developed enough theoretically to be immediately useful for a formal model. If contingency is some degree of freedom and creativity of action - what kind of degree is it precisely? It is evidently not total randomness and chaos. There are some elements of randomness, emergence, and creativity in human behavior, but people also follow habits, laws, and expectations. So how and where does contingency come into play when humans act? How is contingency expanded and constrained by limiting factors around an actor? And finally: How could we formalize these hypotheses about contingent human behavior?

Like we have seen with concepts such as 'place' or 'time' in data modelling, technical discussions about formalising key historiographic concepts can evolve into very fruitful philosophical discussions about bedrock concepts of historical sciences, that may only appear clear on the surface. Simulative modeling such as ABM might also pose an opportunity to discuss concepts of contingency and historical agency once more.

(caveats:desiderata)=
## What is missing to esablish ABM as a useful method for historians?
Now we want to highlight some desiderata for the application of ABM in historical sciences that are, in our opinion, particularly important for an increased and well-founded usage of the method. Some of the requirements for a successful application of ABM in historical sciences directly relate to the caveats presented above as well as the methodological guidelines we gave in [the previous section](../ch3_practice/caveats.md). 

First of all, the primary goal needs to be to establish a *community of practice*. That means of course to have more historians familiarise themselves with the method and actually apply it, but also creating platforms of exchange and discussion that allow the method to actually develop according to the needs and concerns of historians. 

Some beneficial steps, which we in the ModelSEN project also want to further, might be the following:
 
 - show Agent-based modeling to other historians and explain potentials and problems, identify and communicate how ABM fit historical research and start discussions with practitioners *and* non-practicing historians about theoretical, epistemological and methodological questions regarding ABM and simulations, for example through talks, workshops, and documents such as this.
 - connect work in ABM and other simulation methods with other practitioners/their work by reading them, citing them, by talking to them and sharing thoughts.
 - share and discuss approaches, insights, and resources in regular formats, such as conferences, blogs, github/-lab libraries, shared bibliographies, colloquia, seminars and workshops, or even journals.
 - in the long run, develop standards for designing and communicating historical ABMs, e.g., as extensions of ODD/ODD+ or with a new approach unique to historians' needs, framed in relation to unfolding discussions and practices of data-, algorithm-, theory- and method-criticism in digital history  {cite}`fickers_update_2020`.

If you plan to use Agent-based modeling and want to be part of this developing community of practice, do [get in touch with us](https://ModelSEN.mpiwg-berlin.mpg.de/contact/)!

You can also participate in workshops and seminars or listen to talks we will be holding. For this, follow the [news on our project page](https://ModelSEN.mpiwg-berlin.mpg.de/news/) and check out or [compendium page](https://ModelSEN.mpiwg-berlin.mpg.de/compendium) for other further resources regarding ABM and our other research.

**********

### Section Bibliography

```{bibliography}
:filter: docname in docnames
```