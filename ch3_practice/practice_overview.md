# Agent-based Modeling in Practice

In this chapter, we want to give you some guidelines on how you could approach building your own ABMs. There are different methodologies and some of it comes down to a research field's demands and traditions as well as personal research preferences. Additionally, there is no established historical ABM methodology. One goal of this compendium is to give a stepping stone for the development of methodology, so do take this also as an invitation to criticise and discuss those! 

```{note}
This chapter is purely methodological! If you want to get to an actual coding example, skip to our 'Mesa' Tutorial, which introduces you to the Mesa package for [coding your own ABMs](../ch5_mesa/mesa_overview.md).
````

For this reason, we also dedicate one section to the importance of modeling standards and suggestions how deal with the current lack of established standards in history.

The last section of this chapter is about caveats in using ABM, important discussions as well as desiderata that should be addressed to establish a *community of practice* in ABM for historians.


```{tableofcontents}
```