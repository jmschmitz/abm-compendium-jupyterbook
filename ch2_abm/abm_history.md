# Why ABM in history?
The general description of ABM might have given you an idea already why this method might be of interest for historians. The following are some of the main reasons why it is well suited for historical research in particular. 

**1) Modeling (Complex) Dynamics and Processes of History**
    
ABM pushes into a gap that other currently discussed digital humanities methods leave open, which are often concerned with static data and not the dynamics that connect different data points. A lot of historical inquiry however is revolving around dynamical, complex situations that change over time, as well as around corresponding questions of why and how these situations occurred in a specific way. Current DH methods and purely hermeneutic historical research, too, are not well equipped to model or analyze such time-sensitive dynamics of complex historical systems. 

**2) Modeling heterogenous historical agents**
    
ABM allows a huge amount of variability and freedom in modeling the behavior and interactions of agents. This makes ABM appealing for historians, who traditionally put a lot of emphasis on complex and creative behavior of human agents. That focus sets ABM apart from solely mathematical simulation methods which are usually less or even *un*able to model heterogeneous individual behavior, as explained briefly above. 
    
**3) Theory-agnostic modeling of historical behaviors**
    
ABM makes it possible to model assumptions and theories about past behavior of diverse sets of agents with a diverse set of possible interactions. Contrary to a common conception, these assumptions and theories are not limited to a rational-choice paradigm at all (although there exists a large body of research using it for ABM). Of course, the formal modeling of behavior sets technical boundaries to the types of theories that can be represented, so the actual degree of freedom to use a specfic theory needs to be determined in practice.

**4) Extensive body of research in adjacent fields**
    
Not only might this method give a unique perspective to history, it is also tried and tested in many research fields adjacent to history, such as Archaeology, Sociology, Philosophy, or Economics. Especially the first field, Archaeology, has a lot to offer in terms of methodological and epistemological discussions, lessons learned, and specific application questions. A good place to get familiar with some discussions of ABM in Archaeology that are of interest for historians might be {cite}`wurzer_agent-based_2015`. 

## How to know if ABM could be useful
As we said in the introduction on ABM, the method can provide perspectives for certain research questions, but might be less useful for others. ABM might be a useful method for your own research, if...

- ...interactions between entities play an important role in your research.
- ...the individual choices agents in a historical process perform are of importance
- ...you are interested in a macro-level process and how it interacts with the micro-level of individuals' decisions, or vice versa
- ...you are interested in dynamic changes of a system (institutions, networks, regions, practices, etc.)

Learn more about how to start ABM in the next chapter on [ABM in practice](../ch3_practice/practice_overview). For desiderata, caveats, and discussions in using ABM in history, check out [this section](../ch3_practice/caveats).
