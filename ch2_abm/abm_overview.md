# Introduction to ABM
In this chapter we want to introduce you to the simulation method of Agent-based modeling.

In the first section, we try to define Agent-based modeling and differentiate it from other simulation methods. In the second section, we give arguments as to why we think Agent-based modeling is useful for historical research. We also give some basic guidance for you to decide if Agent-based modeling would fit your particular research interest. 

Click the section you are interested in or just start from the top:

```{tableofcontents}
```