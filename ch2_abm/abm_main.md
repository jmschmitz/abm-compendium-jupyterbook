
# What is Agent-based Modeling?
Agent-based modeling (ABM for short) is a simulation method where interactions between agents of a system and the interplay of these interactions with an environment are modeled and simulated. These interactions should ideally produce emergent patterns that may correspond to real-world phenomena. Agents of the real system need to be explicitly and individually represented in the model.

The goal of this method is to link the emergent patterns and phenomena at the systemic *macro*-level with the individual *micro*-level behavior of the agents. 

Agent-based modeling is a form of individual-based (sometimes also called micro-level) approach that differs from equation-based (or macro-level) approaches, in which only macro processes and at most aggregated groups of actors are modeled, not single agents or individuals {cite}`davidsson_types_2017`. Agent-based modeling in turn differs from other micro-level approaches in that each agent is "explicitly and individually represented in the model" and the intensity and variability of the interaction between the agents is particularly high. Apart from this explicit, individual representation, "no further assumptions are made." {cite}`galan_checking_2017`.

While mathematics can still play a major role in it, ABM is not as dependent on equations as other approaches and hence is more flexible. Purely mathematical approaches, while very effective, are limiting in a number of ways that are especially relevant for history. Some of the aspects that are much harder to model mathematically rather than with programming languages are agent heterogeneity, interdependencies between processes, out-of-equilibrium dynamics, local interactions, the role of physical space and finally the micro-macro link, so how macro-level phenomena are interconnected to individual behavior {cite}`epstein_growing_1996`.

*************

## Section Bibliography

```{bibliography}
:filter: docname in docnames
```
